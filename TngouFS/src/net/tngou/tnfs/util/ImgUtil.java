package net.tngou.tnfs.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;








import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 *  图片操作
 * @author tngou
 *
 */
public class ImgUtil {
	private static final Logger log= LoggerFactory.getLogger(ImgUtil .class);

	
	/**
	 * 下载图片
	 * @param imgUrl 图片地址
	 * @param savePath 保存地址
	 * @param saveUrl 保存的URL地址
	 * @return
	 */
	public static String DownImg(String imgUrl,String savePath ,String saveUrl,String name) {
	
		//取得今天时间
	
			DateTimeFormatter f = DateTimeFormatter.ofPattern("yyMMdd");
			LocalDate date =LocalDate.now();
	        String ymd = date.format(f);
			savePath += ymd + File.separator;
			saveUrl += ymd + File.separator;
			File dirFile = new File(savePath); //如果今天的目录存储就创建
			if (!dirFile.exists()) {
				dirFile.mkdirs();
			}
			
		 try {
					
				URL ImgUrl= new URL(imgUrl);
				String ext = FilenameUtils.getExtension(imgUrl).toLowerCase();					
				   if(StringUtils.isEmpty(ext))ext="jpg";
					if(StringUtils.isEmpty(name)) name=DigestMD.MD5(imgUrl);
					name = name +"."+ext ;				
					//取得今天时间			
					File fileDest = new File(savePath, name);
//					FileOutputStream fos = new FileOutputStream(fileDest);
					//FileUtils.copyFile(imgData, fos);
					HttpConfig httpConfig = HttpConfig.getInstance();
					 FileUtils.copyURLToFile(ImgUrl, fileDest, httpConfig.getTimeout(), httpConfig.getTimeout());
					//IOUtils.copy(imgData, fos);	
					 if(!fileDest.isFile())return null;
					 BufferedImage bufferedImage = ImageIO.read(fileDest);
					 int w = bufferedImage.getWidth();
					 int h = bufferedImage.getHeight();
					 if(w<httpConfig.getMinwidth()&&h<httpConfig.getMinheight())  //小图标进行过滤  如：最小 80x60
					 {
						 fileDest.delete();
						 return null;
					 }
					 
					saveUrl  += name;
				

		   return saveUrl;
	   } catch (Exception e) {
		   log.error("现在图片地址-{}-出错",imgUrl);
		   e.printStackTrace();
		   return null;
		}
		
		
		
	}
	
	
	public static void main(String[] args) throws IOException {
		File fileDest=new File("D:/123.jpg");
		URL ImgUrl=new URL("http://www.yi18.net/img/news/20141128165650_647.jpg");
		DownImg("http://www.yi18.net/img/news/20141128165650_647.jpg", "D:/test/", "tsts", "");
	}
	
	
}
