


package net.tngou.tnfs.util;   


import java.io.IOException;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Http请求  get与Post
 * @author 陈磊
 *
 */

public class HttpUtil {

	private static final Logger log= LoggerFactory.getLogger(HttpUtil.class);
	private static HttpConfig httpConfig = HttpConfig.getInstance();
	
	
	/**
	 * 取得页面信息
	* <p>Title: get<／p>
	* <p>Description: <／p>
	* @param url
	* @return
	 */
	public static Document get(String url) {
		Document doc = null;
		
		try {
			doc = Jsoup.connect(url).
					timeout(httpConfig.getTimeout()).
					userAgent(httpConfig.getUserAgent()).
					ignoreContentType(true).
					get();
			Entities.EscapeMode.base.getMap().clear();
		} catch (IOException e) {

//			e.printStackTrace();
			log.error("取得HTTP请求{}出错！",url);
			return null;
		}
		return doc;
		
	}
	
	public static Document post(String url) {
		Document doc = null;
		
		try {
			doc = Jsoup.connect(url).
					timeout(httpConfig.getTimeout()).
					userAgent(httpConfig.getUserAgent()).
					ignoreContentType(true).
					post();
			Entities.EscapeMode.base.getMap().clear();
		} catch (IOException e) {
//			e.printStackTrace();
			log.error("取得HTTP请求{}出错！",url);
			return null;
		}
		return doc;
		
	}
	
	/**
	 * 
	 * @param url   请求的URL
	 * @param data  参数
	 * @return
	 */
	public static Document post(String url,Map<String, String> data) {
		Document doc = null;
		try {
			
			
			doc = Jsoup.connect(url).data(data).
					timeout(httpConfig.getTimeout()).
					userAgent(httpConfig.getUserAgent()).
					ignoreContentType(true).
					post();
		} catch (IOException e) {
//			e.printStackTrace();
			log.error("取得HTTP请求{}出错！",url);
			return null;
		}
		return doc;
		
	}
	
	public static Document get(String url,Map<String, String> data) {
		Document doc = null;
		try {
			
			
			doc = Jsoup.connect(url).data(data).
					timeout(httpConfig.getTimeout()).
					userAgent(httpConfig.getUserAgent()).
					ignoreContentType(true).
					get();
		} catch (IOException e) {
//			e.printStackTrace();
			log.error("取得HTTP请求{}出错！",url);
			return null;
		}
		return doc;
		
	}
	
}
  

